package com.majoriebash.bugsndrugs.util

import com.google.firebase.Timestamp
import org.json.JSONObject
import java.sql.Date
import java.text.SimpleDateFormat
import java.util.*

class Converter {
    fun toDate(date:String): Date {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.UK)
        return Date(dateFormat.parse(date)!!.time)
    }

    fun timeStamptoDate(date:String): Date {
        val obj = JSONObject(date)
        val timestamp = Timestamp(obj.getLong(seconds),obj.getInt(nanoseconds))
        return Date(timestamp.toDate().time)
    }
}