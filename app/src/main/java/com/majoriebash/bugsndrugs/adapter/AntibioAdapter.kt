package com.majoriebash.bugsndrugs.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.majoriebash.bugsndrugs.R

class AntibioAdapter(patients:ArrayList<String>?,sensitive:ArrayList<String>?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<String>? = patients
    private var mRecyclerViewItems1: ArrayList<String>? = sensitive

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.drug)
        var sensitivity:TextView = view.findViewById(R.id.sensitive)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.bioticitem, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder

            val item: String = mRecyclerViewItems!![position]
            val item1: String = mRecyclerViewItems1!![position]
            menuItemHolder.name.text = item
            menuItemHolder.sensitivity.text = item1
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}