package com.majoriebash.bugsndrugs.data.db.entities

import androidx.room.*
import java.sql.Date

@Entity(tableName = "ReportedBugs")
data class ReportedBugs (
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    @ColumnInfo(name = "Unique_Id")
    var uniqueId:String? = null,
    @ColumnInfo(name = "Age")
    var age:Int? = null,
    @ColumnInfo(name = "Gender")
    var gender:String? = null,
    @ColumnInfo(name = "Hospital_Number")
    var hospitalNumber:String? = null,
    @ColumnInfo(name = "Patient_Setting")
    var patientSetting:String? = null,
    @ColumnInfo(name = "Diagnosis")
    var diagnosis:String? = null,
    @ColumnInfo(name = "Specimen_Type")
    var specimentType:String? = null,
    @ColumnInfo(name = "Bugs")
    var bugs:String? = null,
    @ColumnInfo(name = "Antibiotic")
    var antibiotic: String? = null,
    @ColumnInfo(name = "Sensitivity")
    var sensitivity:String? = null,
    var dateCreated: Date? = null,
    @ColumnInfo(name = "Created_By")
    var createdBy:String? = null,
    @ColumnInfo(name="Active")
    var active:Boolean? = null
)