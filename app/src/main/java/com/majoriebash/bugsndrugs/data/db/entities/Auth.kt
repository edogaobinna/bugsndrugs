package com.majoriebash.bugsndrugs.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Date

@Entity(tableName = "auth")
data class Auth (
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var name: String? = null,
    var insitution:String? = null,
    var address:String? = null,
    var profession:String? = null,
    var state:String? = null,
    var email:String? = null,
    var lga: String? = null,
    var role: String? = null,
    var access:String? = null,
    var number:String? = null,
    var dateCreated: Date? = null,
    var activated:Boolean? = null
)