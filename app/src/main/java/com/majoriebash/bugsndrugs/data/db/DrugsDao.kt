package com.majoriebash.bugsndrugs.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.majoriebash.bugsndrugs.data.db.entities.Auth
import com.majoriebash.bugsndrugs.data.db.entities.Drugs

@Dao
interface DrugsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(drugs: Drugs) : Long

    @Query("SELECT * FROM drugs WHERE type = :id ORDER BY name")
    fun getDrugs(id:String) : LiveData<List<Drugs>>
}