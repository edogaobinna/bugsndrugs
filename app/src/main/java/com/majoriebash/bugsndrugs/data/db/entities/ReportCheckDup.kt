package com.majoriebash.bugsndrugs.data.db.entities

import androidx.room.Embedded


data class ReportCheckDup (
    @Embedded
    val reported: ReportedDrugs,
    val institution: String?,
    val phone: String?,
    val lga:String?,
    val state:String?,
    val address:String?,
    val profession:String?
)