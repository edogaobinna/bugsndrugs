package com.majoriebash.bugsndrugs.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.majoriebash.bugsndrugs.data.db.entities.BugCheck
import com.majoriebash.bugsndrugs.data.db.entities.ReportCheckDup
import com.majoriebash.bugsndrugs.data.db.entities.ReportedBugs
import com.majoriebash.bugsndrugs.data.db.entities.ReportedDrugs
import java.time.Duration

@Dao
interface ReportedDrugsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(rDrugs: ReportedDrugs) : Long

    @Query("SELECT * FROM ReportedDrugs WHERE Active=:uid")
    fun getDrug(uid:Boolean) : LiveData<List<ReportedDrugs>>

    @Query("UPDATE ReportedDrugs SET Active = :active WHERE Unique_Id=:uid")
    suspend fun delete(uid:String?,active:Boolean?)

    @Query("UPDATE ReportedDrugs SET Age = :age, Hospital_Number = :hospital, Diagnosis=:diagnosis, Dose=:dose, Duration=:dura, Amount=:amount WHERE Unique_Id=:uid")
    suspend fun update(uid:String?,age:Int?,hospital:String?,diagnosis:String?,dose:String?,dura: String?, amount:String?)

    @Query("SELECT * FROM ReportedDrugs WHERE Unique_Id=:uid")
    fun getDrugs(uid:String?) : LiveData<ReportedDrugs>

    @Query("SELECT ReportedDrugs.*, users.institution as institution,users.number as phone,users.lga as lga,users.state as state, users.address as address, users.profession as profession FROM ReportedDrugs LEFT OUTER JOIN users ON Created_By = Users.email WHERE Active=:uid")
    fun getDrugsForCSV(uid:Boolean) : LiveData<List<ReportCheckDup>>

    @Query("SELECT COUNT(*) FROM ReportedDrugs WHERE Active=:uid")
    fun getDrugsCount(uid:Boolean) : LiveData<Int>

    @Query("SELECT COUNT(*) FROM ReportedDrugs WHERE Gender=:uid AND Active=:active")
    fun getDrugsMale(uid:String,active:Boolean) : LiveData<Int>

    @Query("SELECT COUNT(*) FROM ReportedDrugs WHERE Gender=:uid AND Active=:active")
    fun getDrugsFemale(uid:String,active:Boolean) : LiveData<Int>

    @Query("SELECT COUNT(id) as Total, dateCreated as bugsDate FROM ReportedDrugs WHERE dateCreated<=DATE('now','-7 days') AND Active=:uid GROUP BY dateCreated")
    fun getGroupDrug(uid:Boolean) : LiveData<List<BugCheck>>

    @Query("SELECT COUNT(*) FROM ReportedDrugs WHERE Unique_Id = :uid")
    suspend fun getSingle(uid:String?) : Int
}