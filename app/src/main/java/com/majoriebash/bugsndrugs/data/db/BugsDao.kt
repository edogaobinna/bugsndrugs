package com.majoriebash.bugsndrugs.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.majoriebash.bugsndrugs.data.db.entities.Auth
import com.majoriebash.bugsndrugs.data.db.entities.Bugs

@Dao
interface BugsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(bugs: Bugs) : Long

    @Query("SELECT * FROM bugs ORDER BY name")
    fun getBugs() : LiveData<List<Bugs>>
}