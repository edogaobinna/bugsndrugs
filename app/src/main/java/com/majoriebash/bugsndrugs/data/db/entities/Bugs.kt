package com.majoriebash.bugsndrugs.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "bugs")
data class Bugs (
    @PrimaryKey(autoGenerate = false)
    var id: String = "",
    var name: String? = null,
    var desc:String? = null
)