package com.majoriebash.bugsndrugs.data.firebase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.majoriebash.bugsndrugs.util.Users
import com.majoriebash.bugsndrugs.util.user_name


class Firebase {
    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    fun siginIn(email:String, password:String): LiveData<Any> {
        val isSuccessful = MutableLiveData<Any>()
        mAuth.signInWithEmailAndPassword(email,password)
            .addOnCompleteListener{
                if(it.isSuccessful){
                    db.collection(Users).whereEqualTo(user_name,email).get().addOnCompleteListener {it1->
                        if(it1.isSuccessful){
                            val document = it1.result!!.single()
                            isSuccessful.value = Gson().toJson(document.data)
                        }else{
                            isSuccessful.value = ""
                        }
                    }
                }else{
                    isSuccessful.value = ""
                }
            }
        return isSuccessful
    }

    fun signOut(): LiveData<Any> {
        val isSuccessful = MutableLiveData<Any>()
        mAuth.signOut()
        isSuccessful.value = true
        return isSuccessful
    }

    fun sendPasswordRest(email:String):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener{
            isSuccessful.value = it.isSuccessful
        }
        return isSuccessful
    }

    fun changeUserPassword(oldPass:String, newPass:String?):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        val user:FirebaseUser? = FirebaseAuth.getInstance().currentUser
        val credential = EmailAuthProvider.getCredential(user?.email!!,oldPass)
        user.reauthenticate(credential).addOnCompleteListener {
            if(it.isSuccessful){
                user.updatePassword(newPass!!).addOnCompleteListener{it1->
                    isSuccessful.value = it1.isSuccessful
                }
            }else{
                isSuccessful.value = false
            }
        }
        return isSuccessful
    }


    fun createUser(email: String,password: String,user:HashMap<String,Any?>):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        mAuth.createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener{
                if(it.isSuccessful){
                    db.collection(Users).add(user)
                        .addOnCompleteListener{it1->
                            if(it1.isSuccessful) {
                                isSuccessful.value = it1.isSuccessful
                            }else{
                                isSuccessful.value = false
                            }
                        }
                }else {
                    isSuccessful.value = false
                }
            }
        return isSuccessful
    }

    fun updateDocument(identifier:String,collection:String,field1: String,field: String, value:String):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val document = it.result!!.single().reference
                    document.update(field1,value).addOnCompleteListener{it1->
                        data.value = it1.isSuccessful
                    }
                }else{
                    data.value = false
                }
            }
        return data
    }
    fun updateFieldsDocument(collection:String, field:String,identifier:String,document:HashMap<String,Any?>):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val doc = it.result!!.single().reference
                    doc.update(document)
                }else{
                    data.value = ""
                }
            }
        return data
    }
    fun deactivateAccount(identifier:String,collection:String,field1: String,field: String, value:Boolean):Boolean{
        var data:Boolean? = false
        db.collection(collection).whereEqualTo(field,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val document = it.result!!.single().reference
                    document.update(field1,value).addOnCompleteListener{it1->
                        data = it1.isSuccessful
                    }
                }else{
                    data = false
                }
            }
        return data!!
    }

    fun createDocument(collection:String,document:HashMap<String,Any?>):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        db.collection(collection).add(document)
            .addOnCompleteListener{
                if(it.isSuccessful){
                    isSuccessful.value = it.isSuccessful
                }else {
                    isSuccessful.value = false
                }
            }
        return isSuccessful
    }
    fun createMultipleDocument(collection1:String,document1:HashMap<String,Any?>,
                               collection2:String,document2:HashMap<String,Any?>){
        db.collection(collection1).add(document1)
        db.collection(collection2).add(document2)
    }

    fun createDocumentWithCheck(collection:String,field:String, identifier:String,document:HashMap<String,Any?>):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()==0){
                    db.collection(collection).add(document)
                        .addOnCompleteListener{int->
                            if(int.isSuccessful){
                                isSuccessful.value = int.isSuccessful
                            }else {
                                isSuccessful.value = false
                            }
                        }
                }else{
                    isSuccessful.value = false
                }
            }

        return isSuccessful
    }

    fun getSingleDocument(collection:String, field:String, identifier:String):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val document = it.result!!.single()
                    data.value = Gson().toJson(document.data)
                }else{
                    data.value = ""
                }
            }
        return data
    }

    fun getDocument(collection:String):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(collection).get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    data.value = it.result
                }else{
                    data.value = it.result
                }
            }
        return data
    }

    fun getDocumentIdentifier(collection:String,identifier: String,field:String):LiveData<Any>{
        val data = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier).get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    data.value = it.result
                }else{
                    data.value = it.result
                }
            }
        return data
    }
    fun deleteOutreach(collection:String,field:String, identifier:String):LiveData<Any>{
        val isSuccessful = MutableLiveData<Any>()
        db.collection(collection).whereEqualTo(field,identifier)
            .get()
            .addOnCompleteListener{
                if(it.isSuccessful && it.result!!.size()>0){
                    val document = it.result!!.single().reference
                    document.delete()
                    isSuccessful.value = true
                }else{
                    isSuccessful.value = false
                }
            }

        return isSuccessful
    }
}