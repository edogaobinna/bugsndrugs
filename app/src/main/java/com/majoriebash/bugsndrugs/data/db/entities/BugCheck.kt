package com.majoriebash.bugsndrugs.data.db.entities

import java.sql.Date


data class BugCheck (
    val Total: Int,
    val bugsDate: Date
)