package com.majoriebash.bugsndrugs.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.majoriebash.bugsndrugs.data.db.entities.Auth

@Dao
interface AuthDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(auth: Auth) : Long

    @Query("SELECT * FROM auth WHERE email=:email")
    fun getUser(email:String) : LiveData<Auth>
}