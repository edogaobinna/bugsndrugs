package com.majoriebash.bugsndrugs.data.repositories

import com.majoriebash.bugsndrugs.data.db.AppDatabase
import com.majoriebash.bugsndrugs.data.db.entities.*
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.util.UniqueId

class DataRepository(
    private val db: AppDatabase,
    private val prefs: PreferenceProvider
) {
    suspend fun checkSaveBugs(reportedBugs: ReportedBugs){
        val itemFromDB: Int = getOneRBug(reportedBugs.uniqueId!!)
        if (itemFromDB==0) {
            saveReportedBugs(reportedBugs)
        }
    }
    suspend fun checkSaveDrugs(reportedDrugs: ReportedDrugs){
        val itemFromDB: Int = getOneRDrugs(reportedDrugs.uniqueId!!)
        if (itemFromDB==0) {
            saveReportedDrugs(reportedDrugs)
        }
    }
    suspend fun checkSaveUser(users: Users){
        val itemFromDB: Int = getOneUser(users.email!!)
        if (itemFromDB==0) {
            saveUser(users)
        }
    }

    suspend fun saveAuth(auth: Auth) = db.getAuthDao().insert(auth)
    suspend fun saveUser(users: Users) = db.getUserDao().insert(users)
    suspend fun saveBugs(bugs: Bugs) = db.getBugsDao().insert(bugs)
    suspend fun saveDrugs(drugs: Drugs) = db.getDrugsDao().insert(drugs)
    suspend fun saveReportedBugs(reportedBugs: ReportedBugs) = db.getReportedBugsDao().insert(reportedBugs)
    suspend fun saveReportedDrugs(reportedBugs: ReportedDrugs) = db.getReportedDrugsDao().insert(reportedBugs)


    suspend fun updateRBugs(id:String,age:Int,hospital:String,diagnosis:String) = db.getReportedBugsDao()
        .update(id,age,hospital,diagnosis)
    suspend fun deleteRBugs(id:String,active:Boolean) = db.getReportedBugsDao()
        .delete(id,active)
    suspend fun updateRBDrugs(id:String,age:Int,hospital:String,dianosis:String,dose:String,duration:String,amount:String) = db.getReportedDrugsDao()
        .update(id,age,hospital,dianosis,dose,duration,amount)
    suspend fun deleteRDrugs(id:String,active:Boolean) = db.getReportedDrugsDao()
        .delete(id,active)


    suspend fun getOneRBug(id:String)= db.getReportedBugsDao().getSingle(id)
    suspend fun getOneRDrugs(id:String)= db.getReportedDrugsDao().getSingle(id)
    suspend fun getOneUser(id:String)= db.getUserDao().getSingle(id)

    fun getUser(email:String) = db.getAuthDao().getUser(email)
    fun getUsers() = db.getUserDao().getUsers()
    fun getBugs() = db.getBugsDao().getBugs()
    fun getBugsMale() = db.getReportedBugsDao().getBugsMale("Male",true)
    fun getBugsFemale() = db.getReportedBugsDao().getBugsFemale("Female",true)
    fun getDrugsMale() = db.getReportedDrugsDao().getDrugsMale("Male",true)
    fun getDrugsFemale() = db.getReportedDrugsDao().getDrugsFemale("Female",true)
    fun getReportedBugs() = db.getReportedBugsDao().getBug(true)
    fun getCsvBug() = db.getReportedBugsDao().getBugsForCSV(true)
    fun getCsvDrug() = db.getReportedDrugsDao().getDrugsForCSV(true)
    fun getReportedDrugs() = db.getReportedDrugsDao().getDrug(true)
    fun getReportedBug() = db.getReportedBugsDao().getBugs(prefs.getLastSavedAt(UniqueId))
    fun getReportedDrug() = db.getReportedDrugsDao().getDrugs(prefs.getLastSavedAt(UniqueId))
    fun getDrugs(id:String) = db.getDrugsDao().getDrugs(id)
    fun getBugsCount() = db.getReportedBugsDao().getBugsCount(true)
    fun getDrugsCount() = db.getReportedDrugsDao().getDrugsCount(true)

    fun getGroupBug() = db.getReportedBugsDao().getGroupBug(true)
    fun getGroupDrug() = db.getReportedDrugsDao().getGroupDrug(true)

}