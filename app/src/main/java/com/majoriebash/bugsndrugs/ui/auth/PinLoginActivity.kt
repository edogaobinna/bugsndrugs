package com.majoriebash.bugsndrugs.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.hanks.passcodeview.PasscodeView.PasscodeViewListener
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.databinding.ActivityPinLoginBinding
import com.majoriebash.bugsndrugs.ui.home.HomeActivity
import com.majoriebash.bugsndrugs.util.Pin
import com.majoriebash.bugsndrugs.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class PinLoginActivity : AppCompatActivity(),KodeinAware,PasscodeViewListener {
    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance<AuthViewModelFactory>()
    private val prefs: PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityPinLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_pin_login)
        viewmodel = ViewModelProvider(this,factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this

        binding.passcodeView.localPasscode= prefs.getLastSavedAt(Pin)
        binding.passcodeView.listener = this
        if(prefs.getLastSavedAt(Pin)!!.length<=4){
            Intent(this,LoginActivity::class.java).also{
                startActivity(it)
                finish()
            }
        }
    }

    override fun onSuccess(number: String?) {
        Intent(this, HomeActivity::class.java).also {
            startActivity(it)
        }
    }

    override fun onFail() {
        toast("Incorrect pin entered.")
    }
}
