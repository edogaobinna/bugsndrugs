package com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider

class SettingViewModelFactory(
    private val prefs:PreferenceProvider
): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SettingViewModel(prefs) as T
        }
}