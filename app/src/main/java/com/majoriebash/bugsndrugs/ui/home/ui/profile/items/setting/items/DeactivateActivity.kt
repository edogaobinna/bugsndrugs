package com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting.items

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.QuerySnapshot
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.adapter.UserAdapter
import com.majoriebash.bugsndrugs.data.db.entities.Auth
import com.majoriebash.bugsndrugs.databinding.ActivityDeactivateBinding
import com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting.SettingListener
import com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting.SettingViewModel
import com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting.SettingViewModelFactory
import com.majoriebash.bugsndrugs.util.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class DeactivateActivity : AppCompatActivity(),KodeinAware,SettingListener {
    override val kodein by kodein()

    private val factory: SettingViewModelFactory by instance<SettingViewModelFactory>()
    private lateinit var viewmodel: SettingViewModel
    private var listPatient:ArrayList<Auth>? = ArrayList()
    private lateinit var profileAdapter: UserAdapter
    private lateinit var binding: ActivityDeactivateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Deactivate Accounts"

        binding = DataBindingUtil.setContentView(this,R.layout.activity_deactivate)
        viewmodel = ViewModelProvider(this,factory).get(SettingViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel

        viewmodel.listener = this

        profileAdapter= UserAdapter(listPatient,this)

        viewmodel.getUsers()

        viewmodel.auths.observe(this, Observer {
            if(it.isNotEmpty()){
                listPatient?.clear()
                for(e in it.indices){
                    listPatient?.add(it[e])
                }
                profileAdapter.notifyDataSetChanged()
                binding.emptyState.hide()
                binding.recyclerView.show()
            }else{
                binding.emptyState.show()
                binding.recyclerView.hide()
            }
        })

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = profileAdapter
            addOnItemTouchListener(
                RecyclerTouchListener(
                    context,
                    binding.recyclerView,
                    object : ClickListener {
                        override fun onClick(view: View, position: Int) {
                        }

                        override fun onLongClick(view: View, position: Int) {}
                    })
            )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onStarted(id: Int) {
        TODO("Not yet implemented")
    }

    override fun onSuccess(message: String) {
        toast(message)
    }

    override fun onError(message: String) {
        TODO("Not yet implemented")
    }

    override fun onFirebaseFinish(response: LiveData<Any>, i: Int) {
        if(i==1){
            response.observe(this, Observer {
                viewmodel.processUsers((it as QuerySnapshot))
            })
        }
    }
}
