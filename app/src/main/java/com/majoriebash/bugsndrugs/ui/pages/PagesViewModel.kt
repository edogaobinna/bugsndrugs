package com.majoriebash.bugsndrugs.ui.pages

import android.content.Context
import android.os.Environment
import android.util.Log
import androidx.lifecycle.ViewModel
import com.majoriebash.bugsndrugs.data.db.entities.ReportCheck
import com.majoriebash.bugsndrugs.data.db.entities.ReportCheckDup
import com.majoriebash.bugsndrugs.data.db.entities.ReportedBugs
import com.majoriebash.bugsndrugs.data.db.entities.ReportedDrugs
import com.majoriebash.bugsndrugs.data.firebase.Firebase
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository
import com.majoriebash.bugsndrugs.util.*
import java.io.File
import java.io.FileWriter


class PagesViewModel(
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
):ViewModel() {
    var bugs = repository.getReportedBugs()
    var bugsCSV = repository.getCsvBug()
    var drugsCSV = repository.getCsvDrug()
    var drugs = repository.getReportedDrugs()

    var reportBugs = repository.getReportedBug()
    var reportDrugs = repository.getReportedDrug()

    var pageListener:PageListener? = null


    var age:String? = null
    var diagnosis:String? = null
    var hospital:String? = null
    var dose:String? = null
    var duration:String? = null
    var amount:String? = null

    fun saveRecords(id:String?){
        if(prefs.getLastSavedAt(Switch) == "1"){
            val reported = HashMap<String, Any?>()
            reported[Age] = age
            reported[Hospital] = hospital
            reported[Diagnosis] = diagnosis
            Coroutines.main {
                repository.updateRBugs(id!!,age?.toInt()!!,hospital!!,diagnosis!!)
            }

            Firebase().updateFieldsDocument(reportedBugs, UniqueId, id!!,reported)
        }else{
            val reported = HashMap<String, Any?>()
            reported[Age] = age
            reported[Hospital] = hospital
            reported[Diagnosis] = diagnosis
            reported[Dose] = dose
            reported[Duration] = duration
            reported[Amount] = amount
            Coroutines.main {
                repository.updateRBDrugs(id!!,age?.toInt()!!,hospital!!,diagnosis!!,dose!!,duration!!,amount!!)
            }

            Firebase().updateFieldsDocument(reportedDrugs, UniqueId, id!!,reported)
        }
    }

    fun deleteRecords(id:String?){
        if(prefs.getLastSavedAt(Switch) == "1"){
            val reported = HashMap<String, Any?>()
            reported[active] = false
            Coroutines.main {
                repository.deleteRBugs(id!!,false)
            }

            Firebase().updateFieldsDocument(reportedBugs, UniqueId, id!!,reported)
        }else{
            val reported = HashMap<String, Any?>()
            reported[active] = false
            Coroutines.main {
                repository.deleteRDrugs(id!!,false)
            }

            Firebase().updateFieldsDocument(reportedDrugs, UniqueId, id!!,reported)
        }
    }

    fun exportBugsToCsv(item: List<ReportCheck>,context:Context){

        val dir = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!.toString())
        try {
            val files = dir.listFiles { d, name -> name.endsWith(".csv") }
            if (files!!.isNotEmpty()) {
                files[0].delete()
            }
            val file = File.createTempFile("ReportedBugs", ".csv", dir)
            val csvWrite = CSVWriter()
            csvWrite.CSVWriter(FileWriter(file))
            if(prefs.getLastSavedAt(roleId).equals("owner",true)) {
                val arrStr = arrayOf<String?>("Id", "UniqueId", "Age", "Gender", "Hospital_Number", "Patient_Setting",
                    "Diagnosis", "Specimen_Type", "Bugs", "Antibiotic", "Sensitivity", "Created_By",
                    "Profession","Institution","Address", "Phone Number", "State","LGA", "Date_Created"
                )
                csvWrite.writeNext(arrStr)
                for (e in item.indices) {
                    val antibiotics =
                        item[e].reported.antibiotic?.replace("[", "")?.replace("]", "")?.split(",")
                    val sensitivity =
                        item[e].reported.sensitivity?.replace("[", "")?.replace("]", "")?.split(",")
                    val data = arrayOf(
                        item[e].reported.id.toString(),
                        item[e].reported.uniqueId,
                        item[e].reported.age.toString(),
                        item[e].reported.gender,
                        item[e].reported.hospitalNumber,
                        item[e].reported.patientSetting,
                        item[e].reported.diagnosis,
                        item[e].reported.specimentType,
                        item[e].reported.bugs,
                        antibiotics!![0],
                        sensitivity!![0],
                        item[e].reported.createdBy,
                        item[e].profession,
                        item[e].institution,
                        item[e].address,
                        item[e].phone,
                        item[e].state,
                        item[e].lga,
                        item[e].reported.dateCreated.toString()
                    )
                    csvWrite.writeNext(data)
                    for (i in 1 until antibiotics.size) {
                        val data1: Array<String?>? = arrayOf(
                            " ", " ", " ", " ",
                            " ", " ", " ",
                            " ", " ", antibiotics[i],
                            sensitivity[i], " ", " ", " ", " ", " "
                        )
                        csvWrite.writeNext(data1)
                    }
                }
            }else{
                val arrStr = arrayOf<String?>("Id", "UniqueId", "Age", "Gender", "Hospital_Number", "Patient_Setting", "Diagnosis",
                    "Specimen_Type", "Bugs", "Antibiotic", "Sensitivity", "Created_By", "Date_Created"
                )
                csvWrite.writeNext(arrStr)
                for (e in item.indices) {
                    val antibiotics =
                        item[e].reported.antibiotic?.replace("[", "")?.replace("]", "")?.split(",")
                    val sensitivity =
                        item[e].reported.sensitivity?.replace("[", "")?.replace("]", "")?.split(",")
                    val data = arrayOf(
                        item[e].reported.id.toString(),
                        item[e].reported.uniqueId,
                        item[e].reported.age.toString(),
                        item[e].reported.gender,
                        item[e].reported.hospitalNumber,
                        item[e].reported.patientSetting,
                        item[e].reported.diagnosis,
                        item[e].reported.specimentType,
                        item[e].reported.bugs,
                        antibiotics!![0],
                        sensitivity!![0],
                        item[e].reported.createdBy,
                        item[e].reported.dateCreated.toString()
                    )
                    csvWrite.writeNext(data)
                    for (i in 1 until antibiotics.size) {
                        val data1: Array<String?>? = arrayOf(
                            " ", " ", " ", " ",
                            " ", " ", " ",
                            " ", " ", antibiotics[i],
                            sensitivity[i], " ", " "
                        )
                        csvWrite.writeNext(data1)
                    }
                }
            }
            csvWrite.close()
            pageListener?.openFile(file)
        } catch (sqlEx: Exception) {
            pageListener?.onError()
        }
    }
    fun exportDrugsToCsv(item: List<ReportCheckDup>, context:Context){

        val dir = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!.toString())
        try {
            val files = dir.listFiles { d, name -> name.endsWith(".csv") }
            if (files!!.isNotEmpty()) {
                files[0].delete()
            }
            val file = File.createTempFile("ReportedDrugs", ".csv", dir)
            val csvWrite = CSVWriter()
            csvWrite.CSVWriter(FileWriter(file))
            if(prefs.getLastSavedAt(roleId).equals("owner",true)){
                val arrStr = arrayOf<String?>(
                    "Id",
                    "UniqueId",
                    "Age",
                    "Gender",
                    "Hospital_Number",
                    "Patient_Setting",
                    "Diagnosis",
                    "Route",
                    "Drugs",
                    "Dose",
                    "Amount",
                    "Duration",
                    "Frequency",
                    "Created_By",
                    "Profession",
                    "Institution","Address","Phone Number","State","LGA",
                    "Date_Created"
                )
                csvWrite.writeNext(arrStr)
                for (e in item.indices) {
                    val drugs =
                        item[e].reported.drugs?.replace("[", "")?.replace("]", "")?.split(",")
                    val dose =
                        item[e].reported.dose?.replace("[", "")?.replace("]", "")?.split(",")
                    val freg =
                        item[e].reported.frequency?.replace("[", "")?.replace("]", "")?.split(",")
                    val duration =
                        item[e].reported.duration?.replace("[", "")?.replace("]", "")?.split(",")
                    val route =
                        item[e].reported.route?.replace("[", "")?.replace("]", "")?.split(",")
                    val data = arrayOf(
                        item[e].reported.id.toString(),
                        item[e].reported.uniqueId,
                        item[e].reported.age.toString(),
                        item[e].reported.gender,
                        item[e].reported.hospitalNumber,
                        item[e].reported.patientSetting,
                        item[e].reported.diagnosis,
                        route!![0],
                        drugs!![0],
                        dose!![0],
                        item[e].reported.amount,
                        duration!![0],
                        freg!![0],
                        item[e].reported.createdBy,
                        item[e].profession,
                        item[e].institution,item[e].address,item[e].phone,
                        item[e].state,item[e].lga,
                        item[e].reported.dateCreated.toString()
                    )
                    csvWrite.writeNext(data)
                    for (i in 1 until drugs.size) {
                        val data1:Array<String?>? = arrayOf(
                            " ", " ", " ", " ",
                            " ", " ", " ",
                            route[i], drugs[i], dose[i],"",
                            duration[i], freg[i], " ", " "," "," "," "
                        )
                        csvWrite.writeNext(data1)
                    }
                }
            }else {
                val arrStr = arrayOf<String?>(
                    "Id",
                    "UniqueId",
                    "Age",
                    "Gender",
                    "Hospital_Number",
                    "Patient_Setting",
                    "Diagnosis",
                    "Route",
                    "Drugs",
                    "Dose",
                    "Amount",
                    "Duration",
                    "Frequency",
                    "Created_By",
                    "Date_Created"
                )
                csvWrite.writeNext(arrStr)
                for (e in item.indices) {
                    val drugs =
                        item[e].reported.drugs?.replace("[", "")?.replace("]", "")?.split(",")
                    val dose =
                        item[e].reported.dose?.replace("[", "")?.replace("]", "")?.split(",")
                    val freg =
                        item[e].reported.frequency?.replace("[", "")?.replace("]", "")?.split(",")
                    val duration =
                        item[e].reported.duration?.replace("[", "")?.replace("]", "")?.split(",")
                    val route =
                        item[e].reported.route?.replace("[", "")?.replace("]", "")?.split(",")
                    val data = arrayOf(
                        item[e].reported.id.toString(),
                        item[e].reported.uniqueId,
                        item[e].reported.age.toString(),
                        item[e].reported.gender,
                        item[e].reported.hospitalNumber,
                        item[e].reported.patientSetting,
                        item[e].reported.diagnosis,
                        route!![0],
                        drugs!![0],
                        dose!![0],
                        item[e].reported.amount,
                        duration!![0],
                        freg!![0],
                        item[e].reported.createdBy,
                        item[e].reported.dateCreated.toString()
                    )
                    csvWrite.writeNext(data)
                    for (i in 1 until drugs.size) {
                        val data1:Array<String?>?= arrayOf(
                            " ", " ", " ", " ",
                            " ", " ", " ",
                            route[i], drugs[i], dose[i],"",
                            duration[i], freg[i], " ", " "
                        )
                        csvWrite.writeNext(data1)
                    }
                }
            }
            csvWrite.close()
            pageListener?.openFile(file)
        } catch (sqlEx: Exception) {
            pageListener?.onError()
        }
    }
}