package com.majoriebash.bugsndrugs.ui.pages

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.databinding.ActivityEditRecordBinding
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class EditRecordActivity : AppCompatActivity(),KodeinAware {

    override val kodein by kodein()

    private val factory: PagesViewModelFactory by instance<PagesViewModelFactory>()
    private val prefs: PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: PagesViewModel
    private var uniqueId:String? = null
    private lateinit var binding: ActivityEditRecordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Edit Records"

        binding = DataBindingUtil.setContentView(this,R.layout.activity_edit_record)
        viewmodel = ViewModelProvider(this,factory).get(PagesViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel

        viewmodel.reportBugs.observe(this, Observer {
            if(it!=null){
                uniqueId = it.uniqueId
                binding.age.setText(it.age.toString())
                binding.diagnosis.setText(it.diagnosis)
                binding.hospital.setText(it.hospitalNumber)
                binding.gender.setText(it.gender)
                binding.patient.setText(it.patientSetting)
                binding.specimen.setText(it.specimentType)
                binding.bugs.setText(it.bugs)
                binding.antibiotics.setText(it.antibiotic)
                binding.sensitive.setText(it.sensitivity)
                binding.drugsHold.hide()
                binding.doseHold.hide()
                binding.freqHold.hide()
                binding.duraHold.hide()
                binding.routeHold.hide()
            }
        })
        viewmodel.reportDrugs.observe(this, Observer {
            if(it!=null){
                uniqueId = it.uniqueId
                binding.age.setText(it.age.toString())
                binding.diagnosis.setText(it.diagnosis)
                binding.gender.setText(it.gender)
                binding.hospital.setText(it.hospitalNumber)
                binding.patient.setText(it.patientSetting)
                binding.route.setText(it.route)
                binding.specHold.hide()
                binding.bugHold.hide()
                binding.antiHold.hide()
                binding.sensHold.hide()
                binding.drugs.setText(it.drugs)
                binding.dose.setText(it.dose)
                binding.frequency.setText(it.frequency)
                binding.duration.setText(it.duration)
                binding.amount.setText(it.amount)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.detail, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        if(id==R.id.save){
            viewmodel.saveRecords(uniqueId)
            toast("Entry updated with new record")
            return true
        }
        if(id==R.id.action_search1){
            val dialog = AlertDialog.Builder(this)
            dialog.setTitle("Delete Record")
            dialog.setMessage("Are you sure you want to remove record!!")
            dialog.setCancelable(false)
            dialog.setPositiveButton("Delete", DialogInterface.OnClickListener(){
                    dialogBox, _ ->
                viewmodel.deleteRecords(uniqueId)
                toast("Entry has been removed from records")
                dialogBox.dismiss()
                finish()
            })
            dialog.setNegativeButton("Cancel", DialogInterface.OnClickListener(){
                    dialogBox, _ -> dialogBox.dismiss()
            }).show()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
