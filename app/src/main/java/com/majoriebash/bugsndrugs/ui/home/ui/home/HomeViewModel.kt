package com.majoriebash.bugsndrugs.ui.home.ui.home

import android.view.View
import androidx.lifecycle.ViewModel
import com.anychart.APIlib
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.charts.Cartesian
import com.anychart.core.cartesian.series.Area
import com.anychart.data.Set
import com.anychart.enums.HoverMode
import com.anychart.enums.MarkerType
import com.anychart.enums.ScaleStackMode
import com.anychart.graphics.vector.Stroke
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.majoriebash.bugsndrugs.data.db.entities.BugCheck
import com.majoriebash.bugsndrugs.data.firebase.Firebase
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository
import com.majoriebash.bugsndrugs.ui.home.ui.HomeListener
import com.majoriebash.bugsndrugs.util.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class HomeViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
) : ViewModel() {
    var auth = repository.getUser(prefs.getLastSavedAt(user_name)!!)
    var bugsCount = repository.getBugsCount()
    var drugsCount = repository.getDrugsCount()

    var bugsGroup = repository.getGroupBug()
    var drugsGroup = repository.getGroupDrug()

    var bugsMale = repository.getBugsMale()
    var bugsFemale = repository.getBugsFemale()

    var drugsMale = repository.getDrugsMale()
    var drugsFemale = repository.getDrugsFemale()

    var homeListener:HomeListener? = null

    fun checkInitialized(name:String){
        if(prefs.checkPreference(Initialized)){
            //homeListener?.onStarted()
            if(prefs.getLastBoolean(valid)!!) {
                val response = Firebase().getSingleDocument(Users, user_name,prefs.getLastSavedAt(user_name)!!)
                homeListener?.onFirebaseFinish(response,3)
                prefs.saveBooleanPreference(valid,false)
            }
        }else{
            homeListener?.onSuccess(name)
            val response = Firebase().getDocument(Bugs)
            homeListener?.onFirebaseFinish(response,1)
        }
    }

    fun processBugs(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                val bugs =
                    com.majoriebash.bugsndrugs.data.db.entities.Bugs(it.id,
                        obj.getString(Name),
                        obj.optString(Desc))
                Coroutines.main{
                    repository.saveBugs(bugs)
                }
            }
            val response = Firebase().getDocument(Drugs)
            homeListener?.onFirebaseFinish(response,2)
        }else{
            homeListener?.onError("An error occurred during setup, Please check internet!!")
        }
    }
    fun processDrugs(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                val drugs =
                    com.majoriebash.bugsndrugs.data.db.entities.Drugs(it.id,
                        obj.getString(Name),
                        obj.optString(Desc),
                        obj.getString(Type))
                Coroutines.main{
                    repository.saveDrugs(drugs)
                }
            }
            prefs.saveBooleanPreference(Initialized,true)
            homeListener?.onError("Setup Completed Successfully")
        }else{
            homeListener?.onError("An error occurred during setup, Please check internet!!")
        }
    }
    fun validateUser(user:String){
        if(!user.isNullOrEmpty()) {
            val json = JSONObject(user)
            prefs.saveBooleanPreference(activated,json.getBoolean(activated))
            if(!json.getBoolean(activated)){
                homeListener?.onStarted()
                prefs.deleteAll()
            }
        }
    }

    fun onBugsClick(view: View){
        homeListener?.onClickEvent(1)
    }

    fun onDrugsClick(view: View){
        homeListener?.onClickEvent(2)
    }

    fun plotChart(items:List<BugCheck>,view:AnyChartView,item:String): Cartesian {
        APIlib.getInstance().setActiveAnyChartView(view)
        val areaChart = AnyChart.area()

        areaChart.animation(true)

        val crosshair = areaChart.crosshair()
        crosshair.enabled(true)

        crosshair.yStroke(
            null as Stroke?,
            null,
            null,
            null as String?,
            null as String?
        )
            .xStroke("#fff", 1.0, null, null as String?, null as String?)
            .zIndex(39.0)
        crosshair.yLabel(0).enabled(true)

        areaChart.yScale().stackMode(ScaleStackMode.VALUE)


        val seriesData: MutableList<DataEntry> = ArrayList()
        for (item in items) {
            seriesData.add(
                CustomDataEntry(
                    SimpleDateFormat("EE",Locale.UK).format(item.bugsDate),
                    item.Total.toDouble()
                )
            )
        }

        val set = Set.instantiate()
        set.data(seriesData)
        val series1Data = set.mapAs("{ x: 'x', value: 'value' }")

        val series1: Area = areaChart.area(series1Data)
        series1.stroke("3 #fff")
        series1.name("$item (7 Day Chart)")
        series1.hovered().stroke("3 #fff")
        series1.hovered().markers().enabled(true)
        series1.hovered().markers()
            .type(MarkerType.CIRCLE)
            .size(4.0)
            .stroke("1.5 #fff")
        series1.markers().zIndex(100.0)

        areaChart.legend().enabled(true)
        areaChart.legend().fontSize(13.0)
        areaChart.legend().padding(0.0, 0.0, 20.0, 0.0)


        areaChart.interactivity().hoverMode(HoverMode.BY_X)
        return areaChart
    }

    private class CustomDataEntry internal constructor(x: String?, value: Number?) :
        ValueDataEntry(x, value)
}