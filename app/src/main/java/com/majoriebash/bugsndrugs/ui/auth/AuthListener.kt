package com.majoriebash.bugsndrugs.ui.auth

import androidx.lifecycle.LiveData

interface AuthListener {
    fun onStarted(id:Int)
    fun onSuccess(message:String?)
    fun onError(message:String?)
    fun onStopped()
    fun onFirebaseFinish(response: LiveData<Any>, i: Int)
}