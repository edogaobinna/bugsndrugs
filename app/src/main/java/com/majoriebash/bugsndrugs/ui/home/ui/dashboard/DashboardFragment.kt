package com.majoriebash.bugsndrugs.ui.home.ui.dashboard

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.PersistableBundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.QuerySnapshot
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.adapter.AntibioAdapter
import com.majoriebash.bugsndrugs.databinding.FragmentDashboardBinding
import com.majoriebash.bugsndrugs.ui.home.ui.HomeListener
import com.majoriebash.bugsndrugs.util.ClickListener
import com.majoriebash.bugsndrugs.util.RecyclerTouchListener
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.show
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.util.*


class DashboardFragment : Fragment(),KodeinAware,HomeListener,AdapterView.OnItemSelectedListener {
    private lateinit var binding: FragmentDashboardBinding


    override val kodein by kodein()

    private val factory: DashboardViewModelFactory by instance<DashboardViewModelFactory>()
    private lateinit var viewmodel: DashboardViewModel

    var adapter:ArrayAdapter<String>? = null
    private var listAntibiotics: ArrayList<String>? = ArrayList()
    private var listSensitivity: ArrayList<String>? = ArrayList()
    var antibiotics = MutableLiveData<MutableList<String>>()
    private var outreachAdapter: AntibioAdapter? = AntibioAdapter(listAntibiotics,listSensitivity)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)
        viewmodel =
            ViewModelProvider(this,factory).get(DashboardViewModel::class.java)

        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.homeListener = this

        binding.bugs.onItemSelectedListener = this
        binding.specimen.onItemSelectedListener = this
        binding.patient.onItemSelectedListener = this
        binding.gender.onItemSelectedListener = this
        binding.diagnosis.onItemSelectedListener = this

        binding.antibiotics.setOnClickListener{
            binding.menu.close(true)
            displayDialog()
        }
        binding.save.setOnClickListener{
            binding.menu.close(true)
            val group = binding.textDashboard as ViewGroup
            if(viewmodel.processData(listAntibiotics,listSensitivity)){
                clearFields(group)
            }
        }


        viewmodel.bugs.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            val programs = viewmodel.getBugs(it)
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(requireContext(),
                R.layout.spinner_layout, programs)
            binding.bugs.adapter=adapter
        })
        viewmodel.drugs.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            val programs = viewmodel.getDrugs(it)
            adapter = ArrayAdapter<String>(requireContext(),
                R.layout.spinner_layout, programs)
        })

        antibiotics.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty()){
                //listAntibiotics?.clear()
                outreachAdapter?.notifyDataSetChanged()
                binding.emptyState.hide()
                binding.recyclerView.show()
            }else{
                binding.emptyState.show()
                binding.recyclerView.hide()
            }
        })
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = outreachAdapter
            addOnItemTouchListener(
                RecyclerTouchListener(
                    context,
                    binding.recyclerView,
                    object : ClickListener {
                        override fun onClick(view: View, position: Int) {
                            deleteAntibiotics(position)
                        }

                        override fun onLongClick(view: View, position: Int) {}
                    })
            )
        }
        return binding.root
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent?.id){
            R.id.diagnosis->if(parent.selectedItem.toString().equals("other disease",ignoreCase = true))
            {binding.otherDiagnosis.show() }else{
                binding.otherDiagnosis.hide()
                if(!parent.selectedItem.toString().equals("working diagnosis",ignoreCase = true)){
                    viewmodel.diagnosis = parent.selectedItem.toString()
                }
            }
            R.id.specimen->if(parent.selectedItem.toString().equals("other",ignoreCase = true))
                {binding.otherSpe.show() }else{
                binding.otherSpe.hide()
                    if(!parent.selectedItem.toString().equals("select specimen",ignoreCase = true)){
                        viewmodel.specimen = parent.selectedItem.toString()
                    }
            }
            R.id.patient-> if(!parent.selectedItem.toString().equals("select patient setting",ignoreCase = true)){
                viewmodel.patientSetting = parent.selectedItem.toString()
            }
            R.id.gender-> if(!parent.selectedItem.toString().equals("select sex",ignoreCase = true)){
                viewmodel.gender = parent.selectedItem.toString()
            }
            R.id.bugs->if(parent.selectedItem.toString().equals("other",ignoreCase = true))
            {binding.otherBugs.show() }else{
                binding.otherBugs.hide()
                if(!parent.selectedItem.toString().equals("select bugs",ignoreCase = true)){
                    viewmodel.bugsSelect = parent.selectedItem.toString()
                }
            }
        }
    }

    override fun onStarted() {
        binding.progressBar.show()
    }

    override fun onClickEvent(id: Int) {
        TODO("Not yet implemented")
    }

    override fun onSuccess(name: String?) {
        Toast.makeText(context,name,Toast.LENGTH_SHORT).show()
    }

    override fun onError(message: String?) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
        binding.progressBar.hide()
    }

    override fun onFirebaseFinish(response: LiveData<Any>, id: Int) {
        if(id==1){
            response.observe(this, Observer {
                viewmodel.processBugs((it as QuerySnapshot))
            })
        }
    }

    private fun displayDialog(){
        val view = LayoutInflater.from(context).inflate(R.layout.antibiotics_layout, null) as View
        val spinner:Spinner = view.findViewById(R.id.antibio)
        val spinner1:Spinner = view.findViewById(R.id.sensitive)
        spinner.adapter = adapter
        val dialog = AlertDialog.Builder(context)
        dialog.setTitle("Add Antibiotics")
        dialog.setView(view)
        dialog.setCancelable(false)
        dialog.setPositiveButton("Add", DialogInterface.OnClickListener(){
                dialogBox, _ ->
            if(!spinner.selectedItem.toString().equals("Select Antibiotics",ignoreCase = true)) {
                if(!spinner1.selectedItem.toString().equals("Select Sensitivity",ignoreCase = true)) {
                    if (!listAntibiotics?.contains(spinner.selectedItem.toString())!!) {
                        listAntibiotics?.add(spinner.selectedItem.toString())
                        listSensitivity?.add(spinner1.selectedItem.toString())
                        antibiotics.value = listAntibiotics
                    }
                }
            }
            dialogBox.dismiss()
        })
        dialog.setNegativeButton("Cancel",DialogInterface.OnClickListener(){
                dialogBox, _ -> dialogBox.dismiss()
        }).show()
    }
    private fun deleteAntibiotics(position:Int){
        val dialog = AlertDialog.Builder(context)
        dialog.setTitle("Remove Antibiotics")
        dialog.setMessage("Are you sure you want to remove this item from list!!")
        dialog.setCancelable(false)
        dialog.setPositiveButton("Remove", DialogInterface.OnClickListener(){
                dialogBox, _ ->
            listAntibiotics?.removeAt(position)
            antibiotics.value = listAntibiotics
            listSensitivity?.removeAt(position)
            dialogBox.dismiss()
        })
        dialog.setNegativeButton("Cancel",DialogInterface.OnClickListener(){
                dialogBox, _ -> dialogBox.dismiss()
        }).show()
    }

    private fun clearFields(group:ViewGroup){
        val count = group.childCount
        for( i in 0 until count){
            val view = group.getChildAt(i)
            if (view is EditText) {
                view.text.clear()
            }
            if (view is Spinner) {
                view.setSelection(0)
            }
            if(view is LinearLayout){
                val count1 = view.childCount
                for(y in 0 until count1){
                    val view1 = view.getChildAt(y)
                    if (view1 is EditText) {
                        view1.text.clear()
                    }
                    if (view1 is Spinner) {
                        view1.setSelection(0)
                    }
                }
            }
        }
        listAntibiotics?.clear()
        antibiotics.value = listAntibiotics
        listSensitivity?.clear()
    }

}
