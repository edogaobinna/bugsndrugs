package com.majoriebash.bugsndrugs.ui.auth

import android.view.View
import androidx.lifecycle.ViewModel
import com.google.firebase.messaging.FirebaseMessaging
import com.majoriebash.bugsndrugs.data.db.entities.Auth
import com.majoriebash.bugsndrugs.data.firebase.Firebase
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository
import com.majoriebash.bugsndrugs.util.*
import org.json.JSONObject
import java.sql.Date
import java.util.*
import kotlin.collections.HashMap

class AuthViewModel(
    private val prefs: PreferenceProvider,
    private val repository: DataRepository
):ViewModel() {

    var auth = repository.getUser(prefs.getLastSavedAt(user_name)!!)

    var password:String? = null
    var username:String? = null
    var fullname:String? = null
    var facility:String? = null
    var lga:String? = null
    var number:String? = null
    var profession:String? = null
    var state:String? = null
    var address:String? = null

    var authListener:AuthListener? = null

    fun onRegister(view:View){
        authListener?.onStarted(2)
    }
    fun onLogin(view:View){
        authListener?.onStarted(1)
    }
    fun onResetPassword(view:View){
        authListener?.onStarted(3)
    }

    fun checkAuthentication(){
        prefs.saveBooleanPreference(Reauth,true)
        prefs.deletePreference(Initialized)
        prefs.deletePreference(SetPin)
        prefs.deletePreference(Pin)
    }

    fun onLoginClick(view: View){
        authListener?.onStarted(1)
        if(!username.isNullOrEmpty() && !password.isNullOrEmpty()) {
            if(prefs.checkPreference(user_name) && !prefs.getLastBoolean(Reauth)!!){
                if(password.equals(prefs.getLastSavedAt(pass_word), true)){
                    if(prefs.getLastBoolean(activated)!!){
                        authListener?.onStopped()
                        authListener?.onSuccess("Login Successful")
                    }
                    else{
                        authListener?.onStopped()
                        authListener?.onError("This account is deactivated")
                        prefs.deleteAll()
                    }
                }else{
                    authListener?.onStopped()
                    authListener?.onError("The email and password combination does not exist")
                }
            }else {
                if(prefs.getLastBoolean(Reauth)!!){
                    Firebase().signOut()
                }
                val response = Firebase().siginIn(username!!.trim(), password!!.trim())
                authListener?.onFirebaseFinish(response, 1)
            }
        }
        else{
            authListener?.onError("Email or Password Incorrect")
            authListener?.onStopped()
            return
        }
    }
    fun onRegisterClick(view:View){
        val todayDOB = Date(Calendar.getInstance().timeInMillis)
        authListener?.onStarted(2)
        val user = HashMap<String, Any?>()
        user[name] = fullname
        user[organization] = facility?.toLowerCase()
        user[user_name] = username?.trim()
        user[phone] = number
        user[address_org] = address
        user[profession_org] = profession
        user[state_org] = state
        user[roleId] = "User"
        user[privilege] = "Full Access"
        user[lgas] = lga
        user[date_created] = todayDOB
        user[activated] = true
        if(fullname.isNullOrEmpty() || facility.isNullOrEmpty() || username.isNullOrEmpty() || phone.isNullOrEmpty() ||
            lga.isNullOrEmpty() || password.isNullOrEmpty() || state.isNullOrEmpty() || address.isNullOrEmpty() || profession.isNullOrEmpty()){
            authListener?.onStopped()
            authListener?.onError("Cannot submit with empty fields")
            return
        }
        val response = Firebase().createUser(username!!.trim(),password!!.trim(),user)
        authListener?.onFirebaseFinish(response,1)
    }

    fun loginWithDetails(){
        val response = Firebase().siginIn(username!!.trim(), password!!.trim())
        authListener?.onFirebaseFinish(response, 2)
    }

    fun loginUser(user:String){
        if(!user.isNullOrEmpty()) {
            val json = JSONObject(user)
            val auth =
                Auth(
                    1,
                    json.getString(name),
                    json.getString(organization),
                    json.getString(address_org),
                    json.getString(profession_org),
                    json.getString(state_org),
                    json.getString(user_name).trim(),
                    json.getString(lgas),
                    json.getString(roleId),
                    json.getString(privilege),
                    json.getString(phone),
                    Converter().timeStamptoDate(json.getString(date_created)),
                    json.getBoolean(activated)
                )
            Coroutines.main {
                repository.saveAuth(auth)
                prefs.savePreference(roleId,auth.role)
                prefs.savePreference(user_name,auth.email)
                prefs.savePreference(organization,auth.insitution)
                prefs.savePreference(pass_word,password?.trim())
                prefs.saveBooleanPreference(activated,auth.activated)
                prefs.saveBooleanPreference(Reauth,false)
                if(auth.activated!!) {
                    authListener?.onStopped()
                    authListener?.onSuccess("Login Successful")
                }else{
                    authListener?.onStopped()
                    authListener?.onError("This account is deactivated")
                    prefs.deleteAll()
                }
            }
            FirebaseMessaging.getInstance().subscribeToTopic("BugsnDrugs")
            FirebaseMessaging.getInstance().subscribeToTopic(prefs.getLastSavedAt(organization)!!)
        }else{
            authListener?.onStopped()
            authListener?.onError("Email or Password Incorrect")
        }
    }
    fun resetPassword(email:String?){
        if(email.isNullOrEmpty()){
            return
        }
        val response = Firebase().sendPasswordRest(email)
        authListener?.onFirebaseFinish(response, 2)
    }
}