package com.majoriebash.bugsndrugs.ui.home.ui.profile.items

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.QuerySnapshot
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.databinding.ActivityUpdateBinding
import com.majoriebash.bugsndrugs.ui.home.ui.profile.ProfileListener
import com.majoriebash.bugsndrugs.ui.home.ui.profile.ProfileViewModel
import com.majoriebash.bugsndrugs.ui.home.ui.profile.ProfileViewModelFactory
import com.majoriebash.bugsndrugs.util.Url
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.show
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class UpdateActivity : AppCompatActivity(),KodeinAware,ProfileListener {

    override val kodein by kodein()

    private val factory: ProfileViewModelFactory by instance<ProfileViewModelFactory>()
    private val prefs: PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: ProfileViewModel
    private lateinit var binding: ActivityUpdateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Manage Update"

        binding = DataBindingUtil.setContentView(this, R.layout.activity_update)
        viewmodel = ViewModelProvider(this,factory).get(ProfileViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel
        viewmodel.profileListener = this

        viewmodel.getUpdate()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted(id: Int?) {
        if(id==1) {
            binding.progressBar.hide()
            binding.check.hide()
            binding.updateValue.text = "Hooray!!, Application is up-to-date"
            binding.updateValue.show()
        }else{
            val intent = Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(prefs.getLastSavedAt(Url))
                setPackage("com.android.vending")
            }
            startActivity(intent)
        }
    }

    override fun onSuccess(message: String?) {
        binding.progressBar.hide()
        binding.check.hide()
        binding.updateValue.text = "Version $message is available for update"
        binding.updateValue.show()
        binding.create.show()
        binding.noted.show()
    }

    override fun onError(message: String?) {
        binding.progressBar.hide()
        binding.check.hide()
        binding.updateValue.text = "Oops!!, Seems there is an error checking for updates "
        binding.updateValue.show()
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        response.observe(this, Observer {
            viewmodel.processUpdate(it as QuerySnapshot)
        })
    }
}
