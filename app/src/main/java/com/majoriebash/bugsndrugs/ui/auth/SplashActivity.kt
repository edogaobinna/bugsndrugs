package com.majoriebash.bugsndrugs.ui.auth

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.util.SetPin
import com.majoriebash.bugsndrugs.util.valid
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SplashActivity : AppCompatActivity(),KodeinAware {
    override val kodein by kodein()
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    private var mAuth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        mAuth = FirebaseAuth.getInstance()
        setContentView(R.layout.activity_splash)

        prefs.saveBooleanPreference(valid,true)

        Handler().postDelayed({
            if(prefs.getLastBoolean(SetPin)!!){
                Intent(this, PinLoginActivity::class.java).also {
                    startActivity(it)
                    finish()
                }
            }else{
                Intent(this, LoginActivity::class.java).also {
                    startActivity(it)
                    finish()
                }
            }
        },4000L)
    }
}
