package com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.databinding.ActivitySettingBinding
import com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting.items.DeactivateActivity
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.show
import com.majoriebash.bugsndrugs.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SettingActivity : AppCompatActivity(),KodeinAware,SettingListener {

    override val kodein by kodein()

    private val factory: SettingViewModelFactory by instance<SettingViewModelFactory>()
    private lateinit var viewmodel: SettingViewModel
    private lateinit var binding: ActivitySettingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Manage Settings"

        binding = DataBindingUtil.setContentView(this,R.layout.activity_setting)
        viewmodel = ViewModelProvider(this,factory).get(SettingViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel

        viewmodel.listener = this

        viewmodel.knowRole.observe(this, Observer {
            if(it.equals("user",true)){
                binding.disable.hide()
            }
        })
        viewmodel.knowPin.observe(this, Observer {
            binding.pinSwitch.isChecked = it!!
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted(id: Int) {
        if(id==1){
            val view = LayoutInflater.from(this).inflate(R.layout.paasword_layout, null) as View
            val oldPass: EditText = view.findViewById(R.id.oldPass)
            val newPass: EditText = view.findViewById(R.id.newPass)
            val cPass: EditText = view.findViewById(R.id.cPass)
            val dialog = AlertDialog.Builder(this)
            dialog.setView(view)
            dialog.setCancelable(false)
            dialog.setPositiveButton("Continue", DialogInterface.OnClickListener(){
                    dialogBox, _ ->
                if(oldPass.text.toString().isNullOrEmpty() || newPass.text.toString().isNullOrEmpty()
                    || !newPass.text.toString().equals(cPass.text.toString(),true)) {
                    onError("Error!!!, cannot validate fields")
                }else{
                    viewmodel.changePassword(oldPass.text.toString(), newPass.text.toString())
                    binding.progressBar.show()
                }
            })
            dialog.setNegativeButton("Cancel",DialogInterface.OnClickListener(){
                    dialogBox, _ -> dialogBox.dismiss()
            }).show()
        }
        if(id==2){
            val view = LayoutInflater.from(this).inflate(R.layout.phone_layout, null) as View
            val phone: EditText = view.findViewById(R.id.number)
            val dialog = AlertDialog.Builder(this)
            dialog.setView(view)
            dialog.setCancelable(false)
            dialog.setPositiveButton("Continue", DialogInterface.OnClickListener(){
                    dialogBox, _ ->
                if(phone.text.toString().isNullOrEmpty()) {
                    onError("Error!!!, cannot validate fields")
                }else{
                    viewmodel.changePhone(phone.text.toString())
                    binding.progressBar1.show()
                }
            })
            dialog.setNegativeButton("Cancel",DialogInterface.OnClickListener(){
                    dialogBox, _ -> dialogBox.dismiss()
            }).show()
        }
        if(id==3){
            val view = LayoutInflater.from(this).inflate(R.layout.pin_layout, null) as View
            val phone: EditText = view.findViewById(R.id.number)
            val dialog = AlertDialog.Builder(this)
            dialog.setView(view)
            dialog.setCancelable(false)
            dialog.setPositiveButton("Continue", DialogInterface.OnClickListener(){
                    dialogBox, _ ->
                if(phone.text.toString().isNullOrEmpty()) {
                    onError("Error!!!, cannot validate fields")
                }else{
                   viewmodel.pinSet(phone.text.toString(),binding.pinSwitch.isChecked)
                }
            })
            dialog.setNegativeButton("Cancel",DialogInterface.OnClickListener(){
                    dialogBox, _ -> dialogBox.dismiss()
            }).show()
        }
        if(id==4){
            Intent(this,DeactivateActivity::class.java).also {
                startActivity(it)
            }
        }
    }

    override fun onSuccess(message: String) {
        binding.container.snackbar(message)
        binding.progressBar.hide()
        binding.progressBar1.hide()
    }

    override fun onError(message: String) {
        binding.container.snackbar(message)
        binding.progressBar.hide()
        binding.progressBar1.hide()
    }

    override fun onFirebaseFinish(response: LiveData<Any>, i: Int) {
        if(i==1) {
            response.observe(this, Observer {
                if (it as Boolean) {
                    viewmodel.passwordChanged(it as Boolean)
                }
            })
        }
        if(i==2) {
            response.observe(this, Observer {
                if (it as Boolean) {
                    viewmodel.phoneChanged(it as Boolean)
                }
            })
        }
    }

}
