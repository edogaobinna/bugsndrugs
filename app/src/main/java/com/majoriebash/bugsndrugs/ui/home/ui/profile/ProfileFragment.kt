package com.majoriebash.bugsndrugs.ui.home.ui.profile

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.QuerySnapshot
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.databinding.FragmentProfileBinding
import com.majoriebash.bugsndrugs.databinding.InitializeLayoutBinding
import com.majoriebash.bugsndrugs.ui.auth.LoginActivity
import com.majoriebash.bugsndrugs.ui.home.ui.profile.items.CreateUserActivity
import com.majoriebash.bugsndrugs.ui.home.ui.profile.items.UpdateActivity
import com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting.SettingActivity
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.show
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ProfileFragment : Fragment(),ProfileListener,KodeinAware {

    private lateinit var viewModel: ProfileViewModel
    private lateinit var binding: FragmentProfileBinding

    override val kodein by kodein()

    private val factory: ProfileViewModelFactory by instance<ProfileViewModelFactory>()

    private var alertDialog: AlertDialog.Builder? = null
    private var alert: AlertDialog? = null
    private lateinit var views:InitializeLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        viewModel = ViewModelProvider(this, factory).get(ProfileViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        viewModel.profileListener = this

        viewModel.knowRole.observe(viewLifecycleOwner, Observer {
            if(it.equals("user",true)){
                binding.create.hide()
                binding.createLine.hide()
            }
        })
        viewModel.knowSync.observe(viewLifecycleOwner, Observer {
            if(!it.isNullOrEmpty() && it != "1"){
                binding.synctext.text = "Last attempt: $it"
            }
        })

        return binding.root
    }

    override fun onStarted(id: Int?) {
        if(id==1){
            Intent(context,CreateUserActivity::class.java).also {
                startActivity(it)
            }
        }
        if(id==2){
            Intent(context, SettingActivity::class.java).also {
                startActivity(it)
            }
        }
        if(id==3){
            Intent(context, UpdateActivity::class.java).also {
                startActivity(it)
            }
        }
        if(id==4){
            views = InitializeLayoutBinding.inflate(layoutInflater)
            views.name.hide()
            views.synctext.text = "Please Wait while we download and sync online records"
            alertDialog = AlertDialog.Builder(context)
            alertDialog?.setView(views.root)
            alertDialog?.setCancelable(false)
            alert = alertDialog?.create()
            alert?.show()
        }
    }

    override fun onSuccess(message: String?) {
        if(message.equals("done",true)){
            alert?.dismiss()
            Toast.makeText(context,"Synchronization Completed",Toast.LENGTH_LONG).show()
        }else{
            views.name.show()
            views.name.text = "$message% Completed"
        }
    }

    override fun onError(message: String?) {
        alert?.dismiss()
        Toast.makeText(context,message,Toast.LENGTH_LONG).show()
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        if(id==2) {
            response.observe(this, Observer {
                if (it as Boolean) {
                    if (it) {
                        Intent(context, LoginActivity::class.java).also { it1 ->
                            startActivity(it1)
                        }
                    }
                    viewModel.clearPref()
                }
            })
        }
        if(id==4){
            response.observe(this, Observer {
                viewModel.processReportedBugs((it as QuerySnapshot))
            })
        }
        if(id==5){
            response.observe(this, Observer {
                viewModel.processReportedDrugs((it as QuerySnapshot))
            })
        }
        if(id==6){
            response.observe(this, Observer {
                viewModel.processUsers((it as QuerySnapshot))
            })
        }
    }
}
