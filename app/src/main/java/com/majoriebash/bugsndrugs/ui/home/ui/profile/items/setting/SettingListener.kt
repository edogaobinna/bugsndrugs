package com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting

import androidx.lifecycle.LiveData

interface SettingListener {
    fun onStarted(id:Int)
    fun onSuccess(message:String)
    fun onError(message:String)
    fun onFirebaseFinish(response: LiveData<Any>, i: Int)
}