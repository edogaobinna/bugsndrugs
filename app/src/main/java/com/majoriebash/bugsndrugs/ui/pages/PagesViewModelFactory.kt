package com.majoriebash.bugsndrugs.ui.pages

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository

class PagesViewModelFactory(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return PagesViewModel(repository,prefs) as T
        }
}