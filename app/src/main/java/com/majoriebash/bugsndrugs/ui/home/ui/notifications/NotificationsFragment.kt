package com.majoriebash.bugsndrugs.ui.home.ui.notifications

import android.content.DialogInterface
import android.content.DialogInterface.OnMultiChoiceClickListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.QuerySnapshot
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.adapter.AddDrugsAdapter
import com.majoriebash.bugsndrugs.adapter.AntibioAdapter
import com.majoriebash.bugsndrugs.databinding.FragmentNotificationsBinding
import com.majoriebash.bugsndrugs.ui.home.ui.HomeListener
import com.majoriebash.bugsndrugs.util.ClickListener
import com.majoriebash.bugsndrugs.util.RecyclerTouchListener
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.show
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.util.ArrayList

class NotificationsFragment : Fragment(), KodeinAware, HomeListener, AdapterView.OnItemSelectedListener {

    private lateinit var binding: FragmentNotificationsBinding

    override val kodein by kodein()

    private val factory: NotifyViewModelFactory by instance<NotifyViewModelFactory>()
    private lateinit var viewmodel: NotificationsViewModel
    var adapter: ArrayAdapter<String>? = null
    private var listDrugs: ArrayList<String>? = ArrayList()
    private var listRoute: ArrayList<String>? = ArrayList()
    private var listDose: ArrayList<String>? = ArrayList()
    private var listFreq: ArrayList<String>? = ArrayList()
    private var listDura: ArrayList<String>? = ArrayList()
    var antibiotics = MutableLiveData<MutableList<String>>()
    private var outreachAdapter: AddDrugsAdapter? = AddDrugsAdapter(listDrugs,listRoute,listDose,listFreq,listDura)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false)
        viewmodel =
            ViewModelProvider(this,factory).get(NotificationsViewModel::class.java)

        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.homeListener = this

        binding.diagnosis.onItemSelectedListener = this
        binding.gender.onItemSelectedListener = this
        binding.patient.onItemSelectedListener = this

        binding.drugs.setOnClickListener{
            binding.menu.close(true)
            displayDialog()
        }

        binding.save.setOnClickListener{
            binding.menu.close(true)
            val group = binding.textDashboard as ViewGroup
            if(viewmodel.processData(listDrugs,listDose,listFreq,listDura,listRoute)){
                clearFields(group)
            }
        }

        viewmodel.drugs.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            val programs = viewmodel.getDrugs(it)
            adapter = ArrayAdapter<String>(requireContext(),
                R.layout.spinner_layout, programs)
        })

        antibiotics.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty()){
                //listAntibiotics?.clear()
                outreachAdapter?.notifyDataSetChanged()
                binding.emptyState.hide()
                binding.recyclerView.show()
            }else{
                binding.emptyState.show()
                binding.recyclerView.hide()
            }
        })
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = outreachAdapter
            addOnItemTouchListener(
                RecyclerTouchListener(
                    context,
                    binding.recyclerView,
                    object : ClickListener {
                        override fun onClick(view: View, position: Int) {
                            deleteAntibiotics(position)
                        }

                        override fun onLongClick(view: View, position: Int) {}
                    })
            )
        }

        return binding.root
    }

    override fun onStarted() {

    }

    override fun onClickEvent(id: Int) {
        TODO("Not yet implemented")
    }

    override fun onSuccess(name: String?) {
        Toast.makeText(context,name,Toast.LENGTH_SHORT).show()
        val group = binding.textDashboard as ViewGroup
        clearFields(group)
    }

    override fun onError(message: String?) {
        Toast.makeText(context,message, Toast.LENGTH_SHORT).show()

    }

    override fun onFirebaseFinish(response: LiveData<Any>, id: Int) {
        if(id==1){
            response.observe(this, Observer {
                viewmodel.processDrugs((it as QuerySnapshot))
            })
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent?.id){
            R.id.diagnosis->if(parent.selectedItem.toString().equals("other disease",ignoreCase = true))
            {binding.otherDiagnosis.show() }else{
                binding.otherDiagnosis.hide()
                if(!parent.selectedItem.toString().equals("working diagnosis",ignoreCase = true)){
                    viewmodel.diagnosis = parent.selectedItem.toString()
                }
            }
            R.id.patient-> if(!parent.selectedItem.toString().equals("select patient setting",ignoreCase = true)){
                viewmodel.patientSetting = parent.selectedItem.toString()
            }
            R.id.gender-> if(!parent.selectedItem.toString().equals("select sex",ignoreCase = true)){
                viewmodel.gender = parent.selectedItem.toString()
            }
        }
    }

    private fun displayDialog(){
        val view = LayoutInflater.from(context).inflate(R.layout.drugs_layout, null) as View
        val spinner:Spinner = view.findViewById(R.id.drugs)
        val spinner1:Spinner = view.findViewById(R.id.route)
        val spinner2:Spinner = view.findViewById(R.id.frequency)
        val spinner3:Spinner = view.findViewById(R.id.strength)
        val editText:EditText = view.findViewById(R.id.dose)
        val editText1:EditText = view.findViewById(R.id.duration)
        val editText2:EditText = view.findViewById(R.id.otherDrugs)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = object:AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(parent?.selectedItem.toString().equals("other",ignoreCase = true))
                {editText2.show() }else{
                    editText2.hide()
                }
            }

            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

        }
        val dialog = AlertDialog.Builder(requireContext())
        dialog.setTitle("Add Drugs")
        dialog.setView(view)
        dialog.setCancelable(false)
        dialog.setPositiveButton("Add", DialogInterface.OnClickListener(){
                dialogBox, _ ->
            if(!spinner.selectedItem.toString().equals("Select Drugs",ignoreCase = true)) {
                if(!spinner1.selectedItem.toString().equals("Select Route",ignoreCase = true)) {
                    if(!spinner2.selectedItem.toString().equals("Select Frequency",ignoreCase = true)) {
                        if(editText.text.toString().isNotEmpty()) {
                            if(editText1.text.toString().isNotEmpty()) {
                                if (!listDrugs?.contains(spinner.selectedItem.toString())!!) {
                                    if(!spinner.selectedItem.toString().equals("Other",true)) {
                                        listDrugs?.add(spinner.selectedItem.toString())
                                    }else{
                                        listDrugs?.add(editText2.text.toString())
                                    }
                                    listDose?.add(editText.text.toString()+spinner3.selectedItem.toString())
                                    listFreq?.add(spinner2.selectedItem.toString())
                                    listRoute?.add(spinner1.selectedItem.toString())
                                    listDura?.add(editText1.text.toString())
                                    antibiotics.value = listDrugs
                                }
                            }
                        }
                    }
                }
            }
            dialogBox.dismiss()
        })
        dialog.setNegativeButton("Cancel", DialogInterface.OnClickListener(){
                dialogBox, _ -> dialogBox.dismiss()
        }).show()
    }

    private fun deleteAntibiotics(position:Int){
        val dialog = android.app.AlertDialog.Builder(context)
        dialog.setTitle("Remove Drugs")
        dialog.setMessage("Are you sure you want to remove this item from list!!")
        dialog.setCancelable(false)
        dialog.setPositiveButton("Remove", DialogInterface.OnClickListener(){
                dialogBox, _ ->
            listDrugs?.removeAt(position)
            antibiotics.value = listDrugs
            listRoute?.removeAt(position)
            listDose?.removeAt(position)
            listFreq?.removeAt(position)
            listDura?.removeAt(position)
            dialogBox.dismiss()
        })
        dialog.setNegativeButton("Cancel",DialogInterface.OnClickListener(){
                dialogBox, _ -> dialogBox.dismiss()
        }).show()
    }

    private fun clearFields(group:ViewGroup){
        val count = group.childCount
        for( i in 0 until count){
            val view = group.getChildAt(i)
            if (view is EditText) {
                view.text.clear()
            }
            if (view is Spinner) {
                view.setSelection(0)
            }
        }
        listDrugs?.clear()
        antibiotics.value = listDrugs
        listRoute?.clear()
        listDose?.clear()
        listFreq?.clear()
        listDura?.clear()
    }

}
