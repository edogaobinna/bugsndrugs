package com.majoriebash.bugsndrugs.ui.home.ui.notifications

import android.view.View
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.majoriebash.bugsndrugs.data.db.entities.Drugs
import com.majoriebash.bugsndrugs.data.db.entities.ReportedDrugs
import com.majoriebash.bugsndrugs.data.firebase.Firebase
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository
import com.majoriebash.bugsndrugs.ui.home.ui.HomeListener
import com.majoriebash.bugsndrugs.util.*
import org.json.JSONObject
import java.sql.Date
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class NotificationsViewModel(
    private val prefs: PreferenceProvider,
    private val repository: DataRepository
) : ViewModel() {

    val drugs = repository.getDrugs("2")

    var age:String? = null
    var gender:String? = null
    var patientSetting:String? = null
    var drugSelect:String? = null
    var diagnosis:String? = null
    var hospital:String? = null
    var amount:String? = null

    var homeListener: HomeListener? = null

    fun onRefreshClick(view: View){
        homeListener?.onStarted()
        val response = Firebase().getDocument(Drugs)
        homeListener?.onFirebaseFinish(response,1)
    }

    fun getDrugs(bugsList:List<Drugs>): ArrayList<String> {
        val bugsHash:HashMap<Int,String?> = HashMap()
        val programs = ArrayList<String>()
        programs.add("Select Drugs")
        if(bugsList.isNotEmpty()) {
            for (i in bugsList.indices) {
                bugsHash[i+1] = bugsList[i].id
                programs.add(bugsList[i].name!!)
            }
        }
        programs.add("Other")
        return programs
    }

    fun processDrugs(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                val drugs =
                    Drugs(it.id,
                        obj.getString(Name),
                        obj.optString(Desc),
                        obj.getString(Type))
                Coroutines.main{
                    repository.saveDrugs(drugs)
                }
            }
            prefs.saveBooleanPreference(Initialized,true)
            homeListener?.onError("List refreshed successfully")
        }else{
            homeListener?.onError("Please check internet connection!!")
        }
    }

    fun processData(drugs:ArrayList<String>?, dose:ArrayList<String>?,frequency:ArrayList<String>?,
                            duration:ArrayList<String>?,route:ArrayList<String>?):Boolean{
        if(age.isNullOrEmpty() || gender.isNullOrEmpty() || frequency.isNullOrEmpty() || diagnosis.isNullOrEmpty() || dose.isNullOrEmpty()
            || duration.isNullOrEmpty()  || hospital.isNullOrEmpty() || drugs.isNullOrEmpty()){
            homeListener?.onError("Some fields are empty and cannot be submitted")
            return false
        }else {
            val todayDOB = Date(Calendar.getInstance().timeInMillis)
            val reportedDrugs = ReportedDrugs(
                null, Utility().getUniqueID(), age?.toInt(),gender,hospital,diagnosis,patientSetting,route.toString(),
                drugs.toString(),dose.toString(),duration.toString(),frequency.toString(),todayDOB,
                prefs.getLastSavedAt(user_name),true, amount
            )
            Coroutines.main {
                repository.saveReportedDrugs(reportedDrugs)
            }
            homeListener?.onSuccess("Entry has been saved")
            firebaseJob(reportedDrugs)
            return true
        }
    }

    private fun firebaseJob(documentDrugs:ReportedDrugs){
        val reported = HashMap<String, Any?>()
        reported[UniqueId] = documentDrugs.uniqueId
        reported[Age] = documentDrugs.age
        reported[Gender] = documentDrugs.gender
        reported[Hospital] = documentDrugs.hospitalNumber
        reported[Dose] = documentDrugs.dose
        reported[PatientSetting] = documentDrugs.patientSetting
        reported[Route] = documentDrugs.route
        reported[Duration] = documentDrugs.duration
        reported[Diagnosis] = documentDrugs.diagnosis
        reported[Frequency] = documentDrugs.frequency
        reported[Drugs] = documentDrugs.drugs
        reported[date_created] = documentDrugs.dateCreated
        reported[created_by] = documentDrugs.createdBy
        reported[organization] = prefs.getLastSavedAt(organization)
        reported[active] = true
        reported[Amount] = documentDrugs.amount

        Firebase().createDocument(reportedDrugs,reported)
    }

}