package com.majoriebash.bugsndrugs.ui.home.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository

class HomeViewModelFactory (
    private val prefs:PreferenceProvider,
    private val repository: DataRepository
    ): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return HomeViewModel(repository,prefs) as T
        }
}