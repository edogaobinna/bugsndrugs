package com.majoriebash.bugsndrugs.ui.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository

class AuthViewModelFactory (
    private val prefs:PreferenceProvider,
    private val repository: DataRepository
    ): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AuthViewModel(prefs,repository) as T
        }
}