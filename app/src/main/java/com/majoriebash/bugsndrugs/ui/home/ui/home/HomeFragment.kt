package com.majoriebash.bugsndrugs.ui.home.ui.home

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.anychart.APIlib
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.messaging.FirebaseMessaging
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.data.firebase.Firebase
import com.majoriebash.bugsndrugs.databinding.FragmentHomeBinding
import com.majoriebash.bugsndrugs.ui.auth.LoginActivity
import com.majoriebash.bugsndrugs.ui.home.ui.HomeListener
import com.majoriebash.bugsndrugs.ui.pages.BugsActivity
import com.majoriebash.bugsndrugs.ui.pages.DrugsActivity
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.show
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class HomeFragment : Fragment(),KodeinAware,HomeListener {

    private lateinit var binding: FragmentHomeBinding
    override val kodein by kodein()

    private val factory: HomeViewModelFactory by instance<HomeViewModelFactory>()
    private lateinit var viewmodel: HomeViewModel

    private var alertDialog: AlertDialog.Builder? = null
    private var alert: AlertDialog? = null

    var bugs:Int = 0
    var drugs:Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        //binding.viewmodel = viewmodel
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewmodel =
            ViewModelProvider(this,factory).get(HomeViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel.homeListener = this


        viewmodel.auth.observe(viewLifecycleOwner, Observer {
            if(it.name.isNullOrEmpty()){
                Firebase().signOut()
                Intent(context, LoginActivity::class.java).also { it1 ->
                    startActivity(it1)
                }
            }else {
                viewmodel.checkInitialized(it.name!!)
            }
        })

        viewmodel.bugsCount.observe(viewLifecycleOwner, Observer {
            binding.bugsCount.text = it.toString()
            bugs = it
        })
        viewmodel.drugsCount.observe(viewLifecycleOwner, Observer {
            binding.drugsCount.text = it.toString()
            drugs =it
        })

        viewmodel.bugsGroup.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty()){
                //Toast.makeText(context,it.size.toString(),Toast.LENGTH_LONG).show()
                binding.anyChartView2.setChart(viewmodel.plotChart(it,binding.anyChartView2,"Bugs"))
                binding.anyChartView2.show()
            }
        })
        viewmodel.drugsGroup.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty()){
                //Toast.makeText(context,it.size.toString(),Toast.LENGTH_LONG).show()
                binding.anyChartView1.setChart(viewmodel.plotChart(it,binding.anyChartView1,"Drugs"))
                binding.anyChartView1.show()
            }
        })
    }

    override fun onStarted() {
        Toast.makeText(context,"This account has been deactivated",Toast.LENGTH_LONG).show()
        Intent(context,LoginActivity::class.java).also {
            startActivity(it)
            activity?.finish()
        }
    }

    override fun onClickEvent(id: Int) {
        if(id==1){
            if(bugs>0){
                Intent(context,BugsActivity::class.java).also {
                    startActivity(it)
                }
            }else{
                Toast.makeText(context,"Cannot view empty records",Toast.LENGTH_LONG).show()
            }
        }
        if(id==2){
            if(drugs>0){
                Intent(context,DrugsActivity::class.java).also {
                    startActivity(it)
                }
            }else{
                Toast.makeText(context,"Cannot view empty records",Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onSuccess(name:String?) {
        val view = LayoutInflater.from(context).inflate(R.layout.initialize_layout, null) as View
        val name = view.findViewById<TextView>(R.id.name)
        name.text = "Welcome ${name}"
        alertDialog = AlertDialog.Builder(context)
        alertDialog?.setView(view)
        alertDialog?.setCancelable(false)
        alert = alertDialog?.create()
        alert?.show()
    }

    override fun onError(message:String?) {
        alert?.dismiss()
        Toast.makeText(context,message,Toast.LENGTH_LONG).show()
    }

    override fun onFirebaseFinish(response: LiveData<Any>, id: Int) {
        if(id==1){
            response.observe(this, Observer {
                viewmodel.processBugs((it as QuerySnapshot))
            })
        }
        if(id==2){
            response.observe(this, Observer {
                viewmodel.processDrugs((it as QuerySnapshot))
            })
        }
        if(id==3){
            response.observe(this, Observer {
                viewmodel.validateUser(it as String)
            })
        }
    }

}
