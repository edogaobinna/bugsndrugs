package com.majoriebash.bugsndrugs.ui.home.ui.profile.items

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.databinding.ActivityCreateUserBinding
import com.majoriebash.bugsndrugs.ui.home.ui.profile.ProfileListener
import com.majoriebash.bugsndrugs.ui.home.ui.profile.ProfileViewModel
import com.majoriebash.bugsndrugs.ui.home.ui.profile.ProfileViewModelFactory
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.show
import com.majoriebash.bugsndrugs.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class CreateUserActivity : AppCompatActivity(),KodeinAware,ProfileListener,AdapterView.OnItemSelectedListener {
    override val kodein by kodein()

    private val factory: ProfileViewModelFactory by instance<ProfileViewModelFactory>()
    private lateinit var viewmodel: ProfileViewModel
    private lateinit var binding: ActivityCreateUserBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Add User"

        binding = DataBindingUtil.setContentView(this,R.layout.activity_create_user)
        viewmodel = ViewModelProvider(this,factory).get(ProfileViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel

        binding.role.onItemSelectedListener = this
        binding.privy.onItemSelectedListener=this
        binding.lga.onItemSelectedListener = this
        binding.profession.onItemSelectedListener = this
        binding.state.onItemSelectedListener = this
        viewmodel.profileListener = this
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted(id: Int?) {
        binding.create.hide()
        binding.progressBar.show()
    }

    override fun onSuccess(message: String?) {
        binding.rootLayout.snackbar(message!!)
        binding.create.show()
        binding.progressBar.hide()
    }

    override fun onError(message: String?) {
        binding.rootLayout.snackbar(message!!)
        binding.create.show()
        binding.progressBar.hide()
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        response.observe(this, Observer {
            if(it as Boolean){
                onSuccess("User Account Created Successfully")
            }else{
                onError("Cannot create users at this moment")
            }
        })
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent!!.id){
            R.id.role->if(!parent.selectedItem.toString().equals("select role",ignoreCase = true))
            {viewmodel.role = parent.selectedItem.toString()}
            R.id.privy->if(!parent.selectedItem.toString().equals("Select Privilege Level",ignoreCase = true))
            {viewmodel.access = parent.selectedItem.toString()}
            R.id.lga->if(!parent.selectedItem.toString().equals("select lga",ignoreCase = true))
            {viewmodel.lga = parent.selectedItem.toString()}
            R.id.profession->if(parent.selectedItem.toString().equals("other",ignoreCase = true))
            {binding.professionText.show() }else{
                binding.professionText.hide()
                if(!parent.selectedItem.toString().equals("select profession",ignoreCase = true)){
                    viewmodel.profession = parent.selectedItem.toString()
                }
            }
            R.id.state->if(parent.selectedItem.toString().equals("abia",ignoreCase = true))
            {binding.stateText.hide()
                viewmodel.state = parent.selectedItem.toString()
                binding.lga.show()}else{
                if(!parent.selectedItem.toString().equals("select state",ignoreCase = true)){
                    binding.stateText.show()
                    binding.lga.hide()
                    viewmodel.state = parent.selectedItem.toString()
                }
            }
        }
    }
}
