package com.majoriebash.bugsndrugs.ui.pages

import java.io.File

interface PageListener {
    fun openFile(file: File)
    fun onError()
}