package com.majoriebash.bugsndrugs.ui.home.ui.notifications

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository

class NotifyViewModelFactory(
    private val prefs:PreferenceProvider,
    private val repository: DataRepository
) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return NotificationsViewModel(prefs,repository) as T
        }
}