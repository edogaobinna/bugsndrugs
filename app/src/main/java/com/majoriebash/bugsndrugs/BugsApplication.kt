package com.majoriebash.bugsndrugs

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.multidex.MultiDexApplication
import com.majoriebash.bugsndrugs.data.db.AppDatabase
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository
import com.majoriebash.bugsndrugs.ui.auth.AuthViewModelFactory
import com.majoriebash.bugsndrugs.ui.home.ui.dashboard.DashboardViewModelFactory
import com.majoriebash.bugsndrugs.ui.home.ui.home.HomeViewModelFactory
import com.majoriebash.bugsndrugs.ui.home.ui.notifications.NotifyViewModelFactory
import com.majoriebash.bugsndrugs.ui.home.ui.profile.ProfileViewModelFactory
import com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting.SettingViewModelFactory
import com.majoriebash.bugsndrugs.ui.pages.PagesViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class BugsApplication:MultiDexApplication(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@BugsApplication))

        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { DataRepository(instance(),instance()) }
        bind() from provider { AuthViewModelFactory(instance(),instance()) }
        bind() from provider { ProfileViewModelFactory(instance(),instance()) }
        bind() from provider { HomeViewModelFactory(instance(),instance()) }
        bind() from provider { SettingViewModelFactory(instance()) }
        bind() from provider { DashboardViewModelFactory(instance(),instance()) }
        bind() from provider { NotifyViewModelFactory(instance(),instance()) }
        bind() from provider { PagesViewModelFactory(instance(),instance()) }
    }

}