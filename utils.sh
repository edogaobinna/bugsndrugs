#!/usr/bin/env bash
parse_template(){
    file=$1

    eval "cat <<EOF
$(< ${file})
EOF
"
}

set_version_info(){
    BUILD_NUMBER=${CI_PIPELINE_IID}

    if [[ -z $CI_COMMIT_TAG ]];then
          APP_VERSION_NAME=$(git describe --tags `git rev-list --tags --max-count=1`)
    else
          APP_VERSION_NAME=${CI_COMMIT_TAG}
    fi

    # We don't want the first v for releases
    if [[ $APP_VERSION_NAME == v* ]]; then
        APP_VERSION_NAME=${APP_VERSION_NAME/v/}
    fi

    # Non release tags should have the suffix
    if [[ $CI_COMMIT_TAG != v* ]]; then
        APP_VERSION_NAME=${APP_VERSION_NAME}-${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
    fi

    if [[ $CI_COMMIT_TAG == v* || $CI_COMMIT_REF_SLUG == "master" ]]; then
        VERSION="release"
        GRADLEW="bundleRelease"
    fi
}

generate_keystore(){
    if [[ $VERSION == "release" ]]; then
        echo "${KEYSTORE_FILE_ENCODED}" | base64 -d > ${KEY_STORE_FILE}
    fi
    parse_template key.properties.tpl > key.properties
}
